package ru.demon;

import java.util.Scanner;
/*
*Главный клас позволяющий поиграть в адмирала и потопить вражеский корабль
*
*
 */
public class Game {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args){
        System.out.println("Введите размер канала");
        int channel = scanner.nextInt();
        while(channel < 2 ){
            System.out.println("Вы ввели размер канала куда не помещается корабль, попробуйте ввести размер заного");
            channel = scanner.nextInt();
        }
        Ship ship = new Ship (channel);
        channel++;
        System.out.println("Вы потопили корабль за " + countAndShooting(channel) + " попыток." + " Спасибо за игру");

    }
    /*
    *Метод счетающий количество выстрелов пользователя и реализующий саму стрельбу
    *
    * @return количество выстрелов
     */
    public static int countAndShooting (int channel) {
        boolean result;
         int count = 0;
        do {
            System.out.println("Введите координату места куда будете стрелять" );
            int shot = scanner.nextInt();
            while(shot < 0 || shot >= channel){
                System.out.println("Пробуйте ввести число которое, будет больше 0 и меньше или равно длинне канала ");
                shot = scanner.nextInt();
            }
            result = Ship.shoot(shot);
            count++;
        } while (result != true);
        return count;
    }
}

