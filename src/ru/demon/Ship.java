package ru.demon;

import java.util.ArrayList;

public class Ship{
    public static final String THEY_ALREADY_SHOT_HERE = "Вы уже стреляли в это место";
    public static final String GOT = "Вы попали!";
    public static final String MISS = "Промах";
    public static final String DESTROYED = "Корабль потоплен";
    private static ArrayList<Integer> coordinateShip = new ArrayList<>();
    private static ArrayList coordinateShot = new ArrayList();

    public Ship (int channel){
        int a = (int) (Math.random()*channel);
        coordinateShip.add(a);
        coordinateShip.add(a+1);
        coordinateShip.add(a+2);

    }

    public Ship (){
        coordinateShip.add(1);
        coordinateShip.add(2);
        coordinateShip.add(3);
    }


    public static boolean isDestroyed(int shot) {
        if(coordinateShot.contains(shot)){
            System.out.println(THEY_ALREADY_SHOT_HERE);
        }else{
            if (!coordinateShip.contains(shot)){
                System.out.println(MISS);
                coordinateShot.add(shot);
            }else{
                int x = coordinateShip.indexOf(shot);
                coordinateShip.remove(x);
                coordinateShot.add(shot);
                System.out.println(GOT);
            }
        }
        if (coordinateShip.isEmpty()) {
            System.out.println(DESTROYED);
        }
        return coordinateShip.isEmpty();
    }

}

